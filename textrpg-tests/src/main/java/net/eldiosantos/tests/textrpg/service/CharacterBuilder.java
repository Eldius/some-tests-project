package net.eldiosantos.tests.textrpg.service;

import net.eldiosantos.tests.textrpg.service.character.CharacterBuilderImpl;
import net.eldiosantos.tests.textrpg.service.character.CharacterNameSetter;

/**
 * Created by esjunior on 03/05/2017.
 */
public class CharacterBuilder {

    public CharacterNameSetter start() {
        return new CharacterBuilderImpl();
    }
}

