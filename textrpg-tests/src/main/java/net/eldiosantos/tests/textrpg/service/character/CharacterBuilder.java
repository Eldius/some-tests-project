package net.eldiosantos.tests.textrpg.service.character;

import net.eldiosantos.tests.textrpg.model.Character;

/**
 * Created by esjunior on 03/05/2017.
 */
public interface CharacterBuilder {
    Character build();
}
