package net.eldiosantos.tests.textrpg.service.character;

/**
 * Created by esjunior on 03/05/2017.
 */
public interface HealthCharacterSetter {
    CharacterBuilder health(Long hlt);
    CharacterBuilder health();
}
