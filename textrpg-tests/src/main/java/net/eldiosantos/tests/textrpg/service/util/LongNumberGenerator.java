package net.eldiosantos.tests.textrpg.service.util;

import java.security.SecureRandom;

/**
 * Created by esjunior on 03/05/2017.
 */
public class LongNumberGenerator {
    private final SecureRandom secureRandom = new SecureRandom();

    public Long get() {
        return secureRandom.nextLong() * 100;
    }
}
