package net.eldiosantos.tests.textrpg.service.character;

/**
 * Created by esjunior on 03/05/2017.
 */
public interface DexterityCharacterSetter {
    HealthCharacterSetter dexterity(Long dex);
    HealthCharacterSetter dexterity();
}
