package net.eldiosantos.tests.textrpg.model;

import java.io.Serializable;

/**
 * Created by esjunior on 03/05/2017.
 */
public class Character implements Serializable {
    private String name;
    private Attributes attributes;

    public Character() {
    }

    public Character(String name, Attributes attributes) {
        this.name = name;
        this.attributes = attributes;
    }

    public String getName() {
        return name;
    }

    public Character setName(String name) {
        this.name = name;
        return this;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public Character setAttributes(Attributes attributes) {
        this.attributes = attributes;
        return this;
    }

    @Override
    public String toString() {
        return "Character{" +
                "name='" + name + '\'' +
                ", attributes=" + attributes +
                '}';
    }
}
