package net.eldiosantos.tests.textrpg.service.character;

import net.eldiosantos.tests.textrpg.model.Attributes;
import net.eldiosantos.tests.textrpg.model.Character;
import net.eldiosantos.tests.textrpg.service.util.LongNumberGenerator;

/**
 * Created by esjunior on 03/05/2017.
 */
public class CharacterBuilderImpl implements CharacterNameSetter, CharacterStrengthSetter, DexterityCharacterSetter, HealthCharacterSetter, CharacterBuilder {
    private Character character;
    private Attributes attributes;

    private final LongNumberGenerator generator = new LongNumberGenerator();

    public CharacterBuilderImpl() {
        character = new Character();
        attributes = new Attributes();
        character.setAttributes(attributes);
    }

    @Override
    public CharacterStrengthSetter name(String name) {
        character.setName(name);
        return this;
    }

    @Override
    public DexterityCharacterSetter strength(Long str) {
        attributes.setStr(str);
        return this;
    }

    @Override
    public DexterityCharacterSetter strength() {
        return strength(generator.get());
    }

    @Override
    public HealthCharacterSetter dexterity(Long dex) {
        attributes.setDex(dex);
        return this;
    }

    @Override
    public HealthCharacterSetter dexterity() {
        return dexterity(generator.get());
    }

    @Override
    public CharacterBuilder health(Long hlt) {
        attributes.setHlt(hlt);
        return this;
    }

    @Override
    public CharacterBuilder health() {
        return health(generator.get());
    }

    @Override
    public Character build() {
        final Character response = character;
        character = null;
        attributes = null;
        return response;
    }
}
