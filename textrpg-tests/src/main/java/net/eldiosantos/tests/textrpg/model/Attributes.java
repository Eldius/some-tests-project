package net.eldiosantos.tests.textrpg.model;

import java.io.Serializable;

/**
 * Created by esjunior on 03/05/2017.
 */
public class Attributes implements Serializable {
    private Long str;
    private Long dex;
    private Long hlt;

    public Attributes() {
    }

    public Attributes(Long str, Long dex, Long hlt) {
        this.str = str;
        this.dex = dex;
        this.hlt = hlt;
    }

    public Long getStr() {
        return str;
    }

    public Attributes setStr(Long str) {
        this.str = str;
        return this;
    }

    public Long getDex() {
        return dex;
    }

    public Attributes setDex(Long dex) {
        this.dex = dex;
        return this;
    }

    public Long getHlt() {
        return hlt;
    }

    public Attributes setHlt(Long hlt) {
        this.hlt = hlt;
        return this;
    }

    @Override
    public String toString() {
        return "Attributes{" +
                "str=" + str +
                ", dex=" + dex +
                ", hlt=" + hlt +
                '}';
    }
}
